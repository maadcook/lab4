package com.lab4;

import java.util.Random;

public class Common {
	public static Random rand = new Random();

	public static int random(int min, int max) {
		return min + rand.nextInt(max - min + 1);
	}
}


package com.lab4;

import java.math.BigInteger;

public class Main {

	public static void main(String[] args) {
		// KEY GEN
		
		BigInteger p = BigInteger.valueOf(Common.random(5, 500)).nextProbablePrime();
		BigInteger q = BigInteger.valueOf(Common.random(5, 500)).nextProbablePrime();
		
		BigInteger n = p.multiply(q);
		BigInteger fi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		
		BigInteger e = BigInteger.valueOf(Common.random(5, fi.intValue() / 2)).nextProbablePrime();
		BigInteger d = e.modInverse(fi);
		
		System.out.println("Пара открытых ключей: " + e + ", " + n);
		System.out.println("Пара закрытых ключей: " + d + ", " + n);
		
		
		// ENCRYPT / DECRYPT
		
		BigInteger message = BigInteger.valueOf(1337);
		System.out.println("Cообщение: " + message);
		
		BigInteger crypted = message.pow(e.intValue()).mod(n);
		System.out.println("Зашифрованное сообщение: " + crypted);
		
		BigInteger decrypted = crypted.pow(d.intValue()).mod(n);
		System.out.println("Расшифрованное сообщение: " + decrypted);
	}

}
